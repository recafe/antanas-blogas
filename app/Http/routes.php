<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',['as' => 'home', 'uses' => 'PostController@index']);

Route::get('/home',['as' => 'home', 'uses' => 'PostController@index']);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => ['auth']], function()
{
	Route::get('new-post','PostController@create');

	Route::get('categories-all','CategoriesController@index');


	//Route::resource('categories', 'CategoriesController');

	Route::get('new-category','CategoriesController@create');

	Route::post('new-category','CategoriesController@store');


	Route::post('new-post','PostController@store');

	Route::get('edit/{slug}','PostController@edit');


	Route::get('edit-category/{slug}','CategoriesController@edit');

	Route::post('update-category','CategoriesController@update');


	Route::post('update','PostController@update');
	
	Route::get('delete/{id}','PostController@destroy');

	Route::get('delete-category/{id}','CategoriesController@destroy');


	Route::get('my-all-posts','UserController@user_posts_all');
	
	Route::get('my-drafts','UserController@user_posts_draft');
	
	
	Route::post('comment/add','CommentController@store');
	
	Route::post('comment/delete/{id}','CommentController@distroy');



});


Route::get('user/{id}','UserController@profile')->where('id', '[0-9]+');
Route::get('user/{id}/posts','UserController@user_posts')->where('id', '[0-9]+');

Route::get('category/posts/{id}','CategoriesController@category_posts')->where('id', '[0-9]+');

Route::get('/{slug}',['as' => 'post', 'uses' => 'PostController@show'])->where('slug', '[A-Za-z0-9-_]+');
