<?php namespace App\Http\Controllers;

use App\Posts;
use App\Category;
use App\User;
use Input;
use Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostFormRequest;

use Illuminate\Http\Request;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Posts::where('active','1')->orderBy('created_at','desc')->paginate(5);
		$title = 'Naujausi straipsniai';
		return view('home')->withPosts($posts)->withTitle($title);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		// 
		if($request->user()->can_post())
		{
			$categories = Category::lists('name', 'id');
			return view('posts.create')->with('categories', $categories);

		}	
		else
		{
			return redirect('/')->withErrors('Jūs neturite tam teisių.');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PostFormRequest $request)
	{
		$post = new Posts();
		$post->title = $request->get('title');
		$post->body = $request->get('body');
		$post->slug = str_slug($post->title);
		$post->author_id = $request->user()->id;
		$post->category_id = $request->get('category');

		if($request->has('save'))
		{
			$post->active = 0;
			$message = 'Straipsnis išsaugotas sėkmingai.';
		}			
		else 
		{
			$post->active = 1;
			$message = 'Straipsnis publikuotas sėkmingai';
		}
		$post->save();



		return redirect('edit/'.$post->slug)->withMessage($message);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$post = Posts::where('slug',$slug)->first();

		if($post)
		{
			if($post->active == false)
				return redirect('/')->withErrors('puslapis nerastas');
			$comments = $post->comments;	
		}
		else 
		{
			return redirect('/')->withErrors('puslapis nerastas');
		}
		return view('posts.show')->withPost($post)->withComments($comments);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request,$slug)
	{

		$categories = Category::lists('name', 'id');
		$post = Posts::where('slug',$slug)->first();
		if($post && ($request->user()->id == $post->author_id || $request->user()->is_admin()))

			return view('posts.edit')->with('post',$post)->with('categories',$categories);
		else 
		{
			return redirect('/')->withErrors('jūs neturite teisiu');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		//
		$post_id = $request->input('post_id');
		$post = Posts::find($post_id);
		if($post && ($post->author_id == $request->user()->id || $request->user()->is_admin()))
		{
			$title = $request->input('title');
			$slug = str_slug($title);
			$duplicate = Posts::where('slug',$slug)->first();
			if($duplicate)
			{
				if($duplicate->id != $post_id)
				{
					return redirect('edit/'.$post->slug)->withErrors('Toks pavadinimas jau egzistuoja.')->withInput();
				}
				else 
				{
					$post->slug = $slug;
				}
			}
			
			$post->title = $title;
			$post->body = $request->input('body');
			$post->category_id = $request->get('category');

			if($request->has('save'))
			{
				$post->active = 0;
				$message = 'Straipsnis sėkmingai išsaugotas';
				$landing = 'edit/'.$post->slug;
			}			
			else {
				$post->active = 1;
				$message = 'Straipsnis sėkmingai atnaujintas';
				$landing = $post->slug;
			}
			$post->save();
	 		return redirect($landing)->withMessage($message);
		}
		else
		{
			return redirect('/')->withErrors('jūs neturite tam teisiu');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		//
		$post = Posts::find($id);
		if($post && ($post->author_id == $request->user()->id || $request->user()->is_admin()))
		{
			$post->delete();
			$data['message'] = 'Straipsnis sėkmingai ištrintas';
		}
		else 
		{
			$data['errors'] = 'Jūs neturite tam teisių.';
		}
		
		return redirect('/')->with($data);
	}
}
