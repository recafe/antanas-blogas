<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryFormRequest;
use Redirect;
use App\Category;
use App\Posts;

use Illuminate\Http\Request;

class CategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::all();
		return view('categories.show')->with('categories',$categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('categories.create');

	}

	public function category_posts($id)
	{
		//
		$posts = Posts::where('category_id',$id)->orderBy('created_at','desc')->paginate(5);
		$title = Category::find($id)->name;
		return view('home')->withPosts($posts)->withTitle($title);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateCategoryFormRequest $request)
	{

		$category = new Category;

		$category->name = $request->get('name');

		$category->save();

		return redirect('categories-all')->with('message', 'Kategorija sėkmingai išsaugota!');


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request,$id)
	{
		$category = Category::where('id',$id)->first();
			return view('categories.edit')->with('category',$category);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		//
		$category_id = $request->input('category_id');
		$category = Category::find($category_id);





			$name = $request->input('name');
			$duplicate = Category::where('name',$name)->first();
			if($duplicate)
			{
				if($duplicate->id != $category_id)
				{
					return redirect('edit-category/'.$category->name)->withErrors('Tokia kategorija jau egzistuoja')->withInput();
				}

			}

			$category->name = $name;

			if($request->has('save'))
			{

				$message = 'Kategorija atnaujinta!';

			}
			$category->save();
			return redirect('edit-category/'.$category_id)->withMessage($message);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
		//
		$category = Category::find($id);

			$category->delete();
			$data['message'] = 'Kategorija sėkmingai ištrinta!';

		return redirect('/')->with($data);
	}

}
