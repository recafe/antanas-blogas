@extends('app')

@section('title')
{{ $user->name }}
@endsection

@section('content')
<div>
	<ul class="list-group">
		<li class="list-group-item">
			Registruotas {{$user->created_at->format('Y-m-d H:i:s') }}
		</li>
		<li class="list-group-item panel-body">
			<table class="table-padding">
				<style>
					.table-padding td{
						padding: 3px 8px;
					}
				</style>
				<tr>
					<td>Viso straipsnių</td>
					<td> {{$posts_count}}</td>
					@if($author && $posts_count)
					<td><a href="{{ url('/my-all-posts')}}">Rodyti visus</a></td>
					@endif
				</tr>
				<tr>
					<td>Publikuoti straipsniai</td>
					<td>{{$posts_active_count}}</td>
					@if($posts_active_count)
					<td><a href="{{ url('/user/'.$user->id.'/posts')}}">Rodyti visus</a></td>
					@endif
				</tr>
				<tr>
					<td>Straipsniai juodraštyje</td>
					<td>{{$posts_draft_count}}</td>
					@if($author && $posts_draft_count)
					<td><a href="{{ url('my-drafts')}}">Rodyti visus</a></td>
					@endif
				</tr>
			</table>
		</li>
		<li class="list-group-item">
			Viso komentarų {{$comments_count}}
		</li>
	</ul>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><h3>Naujausi straipsniai</h3></div>
	<div class="panel-body">
		@if(!empty($latest_posts[0]))
		@foreach($latest_posts as $latest_post)
			<p>
				<strong><a href="{{ url('/'.$latest_post->slug) }}">{{ $latest_post->title }}</a></strong>
				<span class="well-sm">{{ $latest_post->created_at->format('Y-m-d H:i:s') }}</span>
			</p>
		@endforeach
		@else
		<p>Jūs nesate parašęs jokio straipsnio šiuo metu.</p>
		@endif
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><h3>Naujausi komentarai</h3></div>
	<div class="list-group">
		@if(!empty($latest_comments[0]))
		@foreach($latest_comments as $latest_comment)
			<div class="list-group-item">
				<p>{{ $latest_comment->body }}</p>
				<p>{{ $latest_comment->created_at->format('Y-m-d H:i:s') }}</p>
				<p><a href="{{ url('/'.$latest_comment->post->slug) }}">{{ $latest_comment->post->title }}</a></p>
			</div>
		@endforeach
		@else
		<div class="list-group-item">
			<p>Komentarų nėra</p>
		</div>
		@endif
	</div>
</div>
@endsection
