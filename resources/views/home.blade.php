@extends('app')

@section('title')
{{$title}}
@endsection

@section('content')

@if ( !$posts->count() )
Kolkas nėra jokių straipsnių šiuo metu
@else
<div class="">
	@foreach( $posts as $post )
	<div class="list-group">
		<div class="list-group-item">
			<h3><a href="{{ url('/'.$post->slug) }}">{{ $post->title }}</a>
				@if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
					@if($post->active == '1')
					<button class="btn" style="float: right"><a href="{{ url('edit/'.$post->slug)}}">Redaguoti straipsnį</a></button>
					@else
					<button class="btn" style="float: right"><a href="{{ url('edit/'.$post->slug)}}">Redaguoti juodraštį</a></button>
					@endif
				@endif
			</h3>
			<p>Publikuota {{ $post->created_at->format('Y-m-d H:i:s') }} autoriaus <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a></p>
			<p>Kategorija: <a href="{{ url('/category/posts/'.$post->category->id)}}">{{ $post->category->name }}</a></p>
		</div>
		<div class="list-group-item">
			<article>
				{!! str_limit($post->body, $limit = 1500, $end = '....... <a href='.url("/".$post->slug).'>Skaityti daugiau</a>') !!}
			</article>
		</div>
	</div>
	@endforeach
	{!! $posts->render() !!}
</div>
@endif

@endsection
