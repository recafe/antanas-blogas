@extends('app')

@section('title')
Redaguoti straipsnį
@endsection

@section('content')
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
			selector: '#post-body'
		});
	</script>

{!! Form::open(array('url' => '/update')) !!}

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="post_id" value="{{ $post->id }}{{ old('post_id') }}">
	<div class="form-group">
		<input required="required" placeholder="Įveskite pavadinimą" type="text" name = "title" class="form-control" value="@if(!old('title')){{$post->title}}@endif{{ old('title') }}"/>
	</div>

<div class="dropdown">
	{{--
            {{ Form::select('locID', array_merge(array('0' => 'Please Select') + $locations), null, array('class' => 'form-control', 'id' => 'locID', Input::old('locID'))) }}
    --}}
	<div class="form-group">

	{!! Form::select('category',($categories = array($post->category_id => $post->category->name) + $categories), null, array('class' => 'form-control')) !!}
</div>



	<div class="form-group">
		<textarea name='body' id="post-body" class="form-control">
			@if(!old('body'))
			{!! $post->body !!}
			@endif
			{!! old('body') !!}
		</textarea>
	</div>
	@if($post->active == '1')
	<input type="submit" name='publish' class="btn btn-success" value = "Atnaujinti"/>
	@else
	<input type="submit" name='publish' class="btn btn-success" value = "Publikuoti"/>
	@endif
	<input type="submit" name='save' class="btn btn-default" value = "Išsaugoti į juodraštį" />
	<a href="{{  url('delete/'.$post->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Ištrinti</a>
{!! Form::close() !!}
@endsection
