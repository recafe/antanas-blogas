@extends('app')

@section('title')
	Pridėti naują straipsnį
@endsection

@section('content')

	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
			selector: '#post-body'
		});
	</script>

	{!! Form::open(array('url' => '/new-post')) !!}

	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group">
		<input required="required" value="{{ old('title') }}" placeholder="Įveskite pavadinimą" type="text" name = "title"class="form-control" />
	</div>

	<div class="dropdown">
		{{--
                {{ Form::select('locID', array_merge(array('0' => 'Please Select') + $locations), null, array('class' => 'form-control', 'id' => 'locID', Input::old('locID'))) }}
        --}}
		<div class="form-group">
			{!! Form::select('category',($categories = array('' => 'Prašome pasirinkti kategoriją') + $categories), null, array('class' => 'form-control')) !!}
		</div>


	</div>

	<div class="form-group">
		<textarea name='body' id="post-body" class="form-control">{{ old('body') }}</textarea>
	</div>
	<input type="submit" name='publish' class="btn btn-success" value = "Publikuoti"/>
	<input type="submit" name='save' class="btn btn-default" value = "Išsaugoti į juodraštį" />
	{!! Form::close() !!}
@endsection
