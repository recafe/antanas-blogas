@extends('app')

@section('title')
	@if($post)
		{{ $post->title }}
		@if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
			<button class="btn" style="float: right"><a href="{{ url('edit/'.$post->slug)}}">Redaguoti straipsnį</a></button>
		@endif
	@else
Puslapis neegzistuoja
	@endif
@endsection

@section('title-meta')
<p>Publikuota {{ $post->created_at->format('Y-m-d H:i:s') }} autoriaus <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a></p>
	<p>Kategorija: {{ $post->category->name }}</p>
@endsection

@section('content')

@if($post)
	<div>
		{!! $post->body !!}
	</div>	
	<div>
		<h2>Parašyti komentarą</h2>
	</div>
	@if(Auth::guest())
		<p>Norėdami komentuoti, prisijunkite</p>
	@else
		<div class="panel-body">
			<form method="post" action="/comment/add">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="on_post" value="{{ $post->id }}">
				<input type="hidden" name="slug" value="{{ $post->slug }}">
				<div class="form-group">
					<textarea required="required" placeholder="Įveskite komentarą" name = "body" class="form-control"></textarea>
				</div>
				<input type="submit" name='post_comment' class="btn btn-success" value = "Skelbti"/>
			</form>
		</div>
	@endif
	
	<div>
		@if($comments)
		<ul style="list-style: none; padding: 0">
			@foreach($comments as $comment)
				<li class="panel-body">
					<div class="list-group">
						<div class="list-group-item">
							<h3>{{ $comment->author->name }}</h3>
							<p>{{ $comment->created_at->format('Y-m-d H:i:s') }}</p>
						</div>
						<div class="list-group-item">
							{{ $comment->body }}
						</div>
					</div>
				</li>
			@endforeach
		</ul>
		@endif
	</div>
@else
404 klaida
@endif

@endsection
