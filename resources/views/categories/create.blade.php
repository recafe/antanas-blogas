@extends('app')

@section('title')
Pridėti naują kategoriją
@endsection

@section('content')


    <form action="/new-category" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <input required="required" value="{{ old('name') }}" placeholder="Įveskite kategorijos pavadinimą" type="text" name = "name" class="form-control" />
        </div>

        <input type="submit" name='save' class="btn btn-default" value = "Išsaugoti" />
    </form>

@endsection
