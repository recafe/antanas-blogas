@extends('app')

@section('title')
    Redaguoti kategoriją
@endsection

@section('content')


    <form method="post" action='{{ url("/update-category") }}'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="category_id" value="{{ $category->id }}{{ old('category_id') }}">
        <div class="form-group">
            <input required="required" placeholder="Įveskite naują kategorijos pavadinimą" type="text" name = "name" class="form-control" value="@if(!old('name')){{$category->name}}@endif{{ old('name') }}"/>
        </div>

        <input type="submit" name='save' class="btn btn-default" value = "Išsaugoti" />
        <a href="{{  url('delete-category/'.$category->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Ištrinti</a>
    </form>

@endsection
