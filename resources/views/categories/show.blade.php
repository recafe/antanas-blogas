@extends('app')

@section('title')
    Kategorijų sąrašas
@endsection
@section('content')

    <div>
        <ul class="list-group">

            <li class="list-group-item panel-body">
                <table class="table-padding">
                    <style>
                        .table-padding td{
                            padding: 3px 8px;
                        }
                    </style>
                    @foreach ($categories as $category)
                        <tr>
                            <td><h3><a href="{{ url('edit-category/'.$category->id)}}">{{ $category->name }}</a></h3></td>
                        </tr>
                    @endforeach
                </table>
            </li>

        </ul>
    </div>

@endsection
